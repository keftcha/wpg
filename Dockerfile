FROM docker.io/golang:1.22 as builder

WORKDIR /usr/src/wpg
COPY . .

RUN CGO_ENABLED=0 go build -o /bin/wpg


FROM alpine

WORKDIR /bin/wpg

COPY --from=builder /bin/wpg .

COPY ./pages pages/
COPY ./static static/

CMD ["./wpg"]
